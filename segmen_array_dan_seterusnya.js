alert ('Hello word')
console.log ('Hello World');
console.error ('This is an error')
console.warn ('This is an warning')

// var, let, const 

const scors = 30;

console.log (scors)

// String, Numbers, Boolean, Null, underfined
const name = 'John';
const age = 30;
const rating = 4.5;
const isCool = true;
const x = null;
const y = undefined;
let z;

console.log(typeof name, typeof age, typeof rating, typeof isCool, typeof x, typeof y, typeof z);


// Constenation
console.log('My name is ' + name + ' and I am ' + age );
// Template String
const hello = `My name is ${name} and I am ${age}`;
console.log(hello);

const s = 'Hello Worlf';

console.log(s.split(''));

// Array - variable that hold multiple values

const fruits = ['apples', 'oranges', 'pears',];

fruits[3] = 'grapes';

fruits.push('mangos');

fruits.unshift('straberries');

fruits.pop();

console.log(Array.isArray('hello'));

console.log(fruits.indexOf('oranges'));   

console.log (fruits);


const person = {
   firstName: 'Jhon',
   lastName: 'Doe',
   age:'30',
   hobbies: ['music', 'movie', 'sports'],
   address: {
      street: '50 main st',
      city: 'Boston',
      state: 'MA'

   }
}

console.log(person);

const todos = [
   {
      id: 1,
      text: 'Take out trash',
      isCompleted: true
   },
   
   {
      id: 2,
      text: 'Meeting wtih boss',
      isCompleted: true
   },
   {
      id: 3,
      text: 'Dentist appt',
      isCompleted: false
   },
]

console.log(todos);

// for
for (let i = 0; i <todos.length; i++){
   console.log(todos[i].text);

}

//forEach, map, filter
todos.forEach(function(todo){
   console.log(todo.text);



});
// Construction Function
function person(firstName, lastName, dob) {
   this.firstName = firstName;
   this.lastName = lastName;
   this.dob = dob;

}

// Class
class person {
   constructor(firstName, lastName, dob) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.dob = new Date(dob);

   }
}

// Instrantiate object
const person1 = new person('John', 'Doe', '4-3-1980');
const person2 = new person('Mary', 'Smith', '3-6-1970');

console.log(person2.firstName);

console.log(window);



// Single element 
console.log(document.getElementById('my-form'));
console.log(document.querySelector('h1'));


// Multiple element
console.log(document.querySelectorAll('.item'));
console.log(document.getElementByClassName('item'));


const items = document.querySelectorAll('.item');

items.forEach((item) => console.log(item));

const ul = document.querySelector('.items')

//ul.remove();
//ul.lastElementChild.remove();
ul.firstElementChild.textContent = 'Hello';
ul.children[1].innerText = 'Brad';
ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

const btn = document.querySelector('.btn');
btn.style.background = 'red';

const btn = document.querySelector('.btn');

btn.addEventListener('click', (e) =>{
   console.log('click');
   e.preventDefault();
   document.querySelector('#my-form').style.background = '#ccc';
   document.querySelector('body').classList.add('bg-dark');
   document.querySelector('.items').lastElementChild.innerHTML = '<h1>Hello</h1>';
});

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);

function onSubmit(e){
   e.preventDefault();

   console.log(nameInput.value);

   if(nameInput.value === '' || emailInput.value === '') {
      alert('please enter fields') 
   } else {
      console.log('success');


   // Clesr Fields
   nameInput.value = '';
   emailInput.value = '';
   }
}





