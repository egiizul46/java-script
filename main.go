package main

import "fmt"

func main() {
	fmt.Println("Welcome to our conference booking application")
	fmt.Println("Get your tickets here to attend")

	var conferenceName = "Go Conference"
	const conferenceTickets = 50
	var remaningTickets = 50

	fmt.Println("Welcome to %v ", conferenceName, "booking application\n")
	fmt.Println("We have total of %v", conferenceTickets, "tickets and", remaningTickets, "are still availabe.\n")
	fmt.Println("Get your tickets here to attend")

	var userName string
	var lastName string
	var email string
	var userTickets int
	// ask user for their name

	fmt.Println("Enter your first name:")
	fmt.scan(&firstName)

	fmt.Println("Enter your last name:")
	fmt.scan(&lastName)

	fmt.Println("Enter your email address:")
	fmt.scan(&email)

	fmt.Println("Enter your number of tickets:")
	fmt.scan(&userTickets)

	fmt.Println("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTickets, email)

}
